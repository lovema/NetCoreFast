﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;
using Model;
using Common;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public interface IBaseDAL<T> where T : ID, new()
    {
        MyDbContext DbContext();
        bool Add(T entity);
        bool Add(IEnumerable<T> entities);        
        bool Delete(T entity);
        bool Delete(int id);
        bool Delete(Expression<Func<T, bool>> where);
        bool Delete(Dictionary<string, string> where);
        bool Update(T entity, params string[] propertyNames);
        bool Update(T entity);
        T SelectOne(int id);
        T SelectOne(Expression<Func<T, bool>> whereLambda);
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="where"></param>
        /// <param name="excludes">排除那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        T SelectOne(Dictionary<string, string> where, string excludes = null);
        /// <summary>
        /// 条件查询，并附带分页
        /// </summary>
        /// <param name="where"></param>
        /// <param name="excludes">排除那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        Pagination<T> Query(Dictionary<string, string> where,string excludes= null);
        DbSet<T> Query();
        IEnumerable<T> SelectAll();
        IEnumerable<T> SelectAll(Expression<Func<T, bool>> where);
        /// <summary>
        /// 查询所有
        /// </summary>
        /// <param name="where"></param>
        /// <param name="excludes">排除那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        IEnumerable<T> SelectAll(Dictionary<string, string> where, string excludes = null);
        Pagination<T> SelectAll(Expression<Func<T, bool>> whereLambda, int pageNo, int pageSize);
        Pagination<T>  SelectAll<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize);

    }
}
